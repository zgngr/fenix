DROP DATABASE IF EXISTS fenix_db;    

CREATE DATABASE fenix_db;

\c fenix_db;

CREATE TABLE "user" (
	id uuid PRIMARY KEY,
	email VARCHAR (120) UNIQUE NOT NULL,
	password VARCHAR (100) NOT NULL
);