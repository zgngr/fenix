 
Running api
```
docker-compose up -d
```

Sign-up user

* **URL**

  localhost:5000/register

* **Method:**
  
  `POST`

* **Body**

  `{ "email" : "test@mail.com" , "password": "123456" }`

* **Success Response:**
  * **Code:** 200 <br />
    **Content:** `{ "message" : "OK" }`
 
* **Error Response:**

  * **Code:** 400 <br />
    **Content:** `{'message': ${email} already exists."}`


* **Sample Call:**

    ```bash
    curl --location --request POST 'http://localhost:5000/register' \
    --header 'Content-Type: application/json' \
    --data-raw '{
        "email": "auser@gmail.com",
        "password": "123456"
    }'
    ```
Login user

* **URL**

  localhost:5000/login

* **Method:**
  
  `POST`

* **Success Response:**
  * **Code:** 200 <br />
    **Content:** `{ "token" : "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjdlMTE4YmFkLWQ5YmItNDBlOC1iYjZlLTFjZWRlN2UwOTZiZiIsImV4cCI6MTYyOTU5OTMzM30.T9wj4zWqBk6hMdWRJUuhjcs6n9QA_MDk50zOlUJ2ca0" }`
 
* **Error Response:**

  * **Code:** 401 <br />
    **Content:** `{"message": "could not verify"}`


* **Sample Call:**

    ```bash
    curl --location --request POST 'http://localhost:5000/login' \ 
         --header 'Authorization: Basic emdlZWVuMkBnbWFpbC5jb206MTIzNDU2'
    ```
Get user

* **URL**

  localhost:5000/me

* **Method:**
  
  `GET`

* **Success Response:**
  * **Code:** 200 <br />
    **Content:** `{ "message": "you are auser@gmail.com"
}`
 
* **Error Response:**

  * **Code:** 401 <br />
    **Content:** `{ "message": "a valid token is missing" }`


* **Sample Call:**

    ```bash
  curl --location --request GET 'http://localhost:5000/me' \
                    --header 'x-access-tokens: eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjEwOTc4MWM1LWYyYjktNGQxNy04MTA0LTc4MmY2YzhiYTdkZiIsImV4cCI6MTYyOTU5NzIwNH0.wGlXkB_oEjQis7VL2QIVnY0U_w-vItVFL6VGM5nlpuM'
    ```

