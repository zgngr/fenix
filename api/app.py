import os
import jwt
import uuid
import datetime
from flask import Flask, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from functools import wraps
from sqlalchemy import exc
from models import User, db
from werkzeug.security import generate_password_hash, check_password_hash

app = Flask(__name__)

app.config['SECRET'] = os.environ.get('SECRET')
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get('DATABASE_URL')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db.init_app(app)

def token_required(f):
    @wraps(f)
    def decorator(*args, **kwargs):

        token = None

        if 'x-access-tokens' in request.headers:
            token = request.headers['x-access-tokens']

        if not token:
            return jsonify({'message': 'a valid token is missing'})
        
        try:
            print('hello')
            data = jwt.decode(token, app.config['SECRET'], algorithms=["HS256"])
            print(data['id'])
            current_user = User.query.filter_by(id=data['id']).first()
        except:
            return jsonify({'message': 'token is invalid'})

        return f(current_user, *args, **kwargs)
    return decorator


@app.route('/register', methods=['POST'])
def signup_user():
    data = request.get_json()
      
    if 'email' not in data or 'password' not in data:
        return jsonify({'message': 'provide email & password.'}), 400
    
    hashed_password = generate_password_hash(data['password'], method='sha256')
    
    try:
        new_user = User(id=str(uuid.uuid4()), email=data['email'], password=hashed_password)
        db.session.add(new_user)  
        db.session.commit()
    except exc.IntegrityError as e:
        db.session.rollback()
        return jsonify({'message': f"{data['email']} already exists."}), 400
    except Exception as e:
        return jsonify({'message': 'internal error.'}), 500
        db.session.rollback()
    finally:
        db.session.bind.dispose()
        
    return jsonify({'message': 'OK'})

@app.route('/login', methods=['POST'])  
def login_user():
     auth = request.authorization
     
     if not auth or not auth.username or not auth.password:
         return jsonify({'message': 'authentication required.'}), 401
     
     user = User.query.filter_by(email=auth.username).first()
     
     if user and check_password_hash(user.password, auth.password):
         token = jwt.encode({'id': str(user.id), 'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=45)}, app.config['SECRET'], "HS256")
         return jsonify({'token': token}), 200
     
     return jsonify({'message': 'could not verify'}), 401
 
@app.route('/me', methods=['GET'])
@token_required
def get_user(current_user):
    return jsonify({'message': f"you are {current_user.email}"})
     

if __name__ == '__main__':
	app.run(host="0.0.0.0", debug=True)